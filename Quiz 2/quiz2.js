//1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
console.log("Soal No 1 - Class Score")
class Score {
    constructor(points) {
           this.subject;
           this.points=points;
           this.email;
       }
       a = () => {
           var no=0;
           for(let a=0;a<=this.points.length-1;a++){
               no=no+this.points[a];
           }
           let xx=no/this.points.length;
           console.log(xx);
       }
   }
   var a=[1,2,3,4,5];
   var average = new Score(a);
   average.a()
console.log(" ")
//2. SOAL Create Score (10 Poin + 5 Poin ES6)
console.log("Soal No 2 - Create Score")
const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]
//console.log(data.length)
function viewScores(data, subject) {
  let arrJudul = data[0];
  let arrIsi = data.slice(1,data.length);
  let arrBalikan = [];
  for (let i=0; i < arrIsi.length; i++){
 	let newObj = {}
 	newObj.email = arrIsi[i][0];
 	newObj.subject = subject;
 	newObj.points = arrIsi[i][arrJudul.indexOf(subject)];
 	arrBalikan.push(newObj);
  }
  console.log(arrBalikan);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")
console.log(" ")
//3. SOAL Recap Score (15 Poin + 5 Poin ES6)
console.log("Soal No 3 - Recap Score")
function recapScores(data) {
    let arrIsi = data.slice(1,data.length);
    for (let i=0; i < arrIsi.length; i++){
        let average = (arrIsi[i][1]+arrIsi[i][2]+arrIsi[i][3])/3;
      let predikat = '';
      if(average > 90){
          predikat = 'honour';
      } else if(average > 80){
          predikat = 'graduate';
      } else if(average > 70){
          predikat = 'participant';
      }
        console.log(i+1 +'. Email: '+arrIsi[i][0]);
        console.log('Rata-rata: ' + average.toFixed(1));
        console.log('Predikat: ' + predikat +'\n');
    }
    
  }
  
  recapScores(data);  