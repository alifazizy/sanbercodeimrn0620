//soal no 1
var word = 'JavaScript'; 
var second = ' is'; 
var third = ' awesome'; 
var fourth = ' and'; 
var fifth = ' I'; 
var sixth = ' love'; 
var seventh = ' it!';
console.log(word + second + third + fourth + fifth + sixth + seventh);

//soal no 2
var sentence = "I am going to be React Native Developer"; 
var FirstWord = sentence[0] ; 
var SecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence [5] + sentence[6] + sentence[7] + sentence[8]+ sentence[9];
var fourthWord = sentence [11] + sentence[12];
var fiftWord = sentence [14] + sentence[15];
var sixthWord = sentence [17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord = sentence [23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eightword = sentence [29] + sentence[30] + sentence[31] + sentence[32] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];
console.log('First Word : ' + sentence[0]); 
console.log('Second Word : ' + sentence[2] + sentence[3]); 
console.log('Third Word : ' + sentence [5] + sentence[6] + sentence[7] + sentence[8]+ sentence[9]); 
console.log('Fourth Word : ' + sentence [11] + sentence[12]); 
console.log('Fifth Word : ' + sentence [14] + sentence[15]); 
console.log('Sixth Word : ' + sentence [17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]); 
console.log('Seventh Word : ' + sentence [23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]);
console.log('Eight Word : ' + sentence [29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]);

//soal no 3
var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);
console.log('First Word2: ' + sentence2.substring(0, 3)); 
console.log('Second Word2: ' + sentence2.substring(4, 14)); 
console.log('Third Word2: ' + sentence2.substring(15, 17)); 
console.log('Fourth Word2: ' + sentence2.substring(18, 20)); 
console.log('Fifth Word2: ' + sentence2.substring(21, 25));

//soal no 4
var sentence3 = 'wow JavaScript is so cool'; 
var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = sentence3.substring(0, 3).length
var secondWordLength = sentence3.substring(4, 14).length
var thirdWordLength = sentence3.substring(15, 17).length
var fourthWordLength = sentence3.substring(18, 20).length
var fifthWordLength = sentence3.substring(21, 25).length
console.log('First Word: ' + sentence3.substring(0, 3) + ', with length: ' + firstWordLength);
console.log('Second Word: ' + sentence3.substring(4, 14) + ', with length: ' + secondWordLength);
console.log('Third Word: ' + sentence3.substring(15, 17) + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + sentence3.substring(18,20) + ', with length: ' + fourthWordLength);
console.log('Fifth Word:' + sentence3.substring(21, 25) + ', with length:' + fifthWordLength);