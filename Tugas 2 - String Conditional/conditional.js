//If-else
var nama = "kosong"
if (nama= "kosong") {
    console.log("Nama harus diisi")
} else if(nama= "Alif") {
    console.log("Hallo Alif, pilih peranmu untuk memulai game")
}
if(peran= "Guard"){
    console.log("Selamat datang di dunia werewolf Alif")
    console.log("Hallo Guard Alif, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
//Switch Case
var hari = 09;
var bulan = 9
var tahun = 2004;
switch (bulan){
    case 1:
        console.log("9 Januari 2004");
        break;
    case 2:
        console.log("9 Februari 2004");
        break;
    case 3:
        console.log("9 Maret 2004");
        break;
    case 4:
        console.log("9 April 2004");
        break;
    case 5:
        console.log("9 Mei 2004");
        break;
    case 6:
        console.log("9 Juni 2004");
        break;
    case 7:
        console.log("9 Juli 2004");
        break;
    case 8:
        console.log("9 Agustus 2004");
        break;
    case 9:
        console.log("9 September 2004");
        break;
    case 10:
        console.log("9 Oktober 2004");
        break;
    case 11:
        console.log("9 November 2004");
        break;
    case 12:
        console.log("9 Desember 2004");
        break;
}