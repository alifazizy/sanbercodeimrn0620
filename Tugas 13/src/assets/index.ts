const LoginIlustration = require('./images/login.png');
const FacebookLogo = require('./images/facebooklogo.png');
const InstagramLogo = require('./images/instagramlogo.png');
const GithubLogo = require('./images/githulogo.png');
const ProfileImage = require('./images/profile.jpg');

export { LoginIlustration };
export { FacebookLogo };
export { GithubLogo };
export { InstagramLogo };
export { ProfileImage };
