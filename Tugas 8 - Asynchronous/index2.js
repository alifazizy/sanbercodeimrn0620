console.log("Soal No. 2");
console.log("Promise Baca Buku");
var readBooksPromise = require('./promise.js')

var books = [
    {name: 'Harry Potter', timeSpent: 3000}, 
    {name: 'Boruto', timeSpent: 2000}, 
    {name: 'Mob Psycho', timeSpent: 4000}
]

var time=10000
var i=0;
function letsread(time,books){
    if(time>0 && i<books.length)
    readBooksPromise(time,books[i])
        .then(function (sisaWaktu) {
            time=sisaWaktu;
            i++;
            letsread(time,books);
        })
        .catch(function (sisaWaktu) {
            return;
        });
}
letsread(time,books)
