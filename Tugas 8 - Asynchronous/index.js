console.log("Soal No. 1");
console.log("Callback Baca Buku");

var readBooks = require('./callback.js')
var books = [
    {name: 'Harry Potter', timeSpent: 3000}, 
    {name: 'Boruto', timeSpent: 2000}, 
    {name: 'Mob Psycho', timeSpent: 4000}
]

var time=10000
var i=0;
function letsread(time,books){
    if(time>0 && i<books.length)
    readBooks(time,books[i],function(sisaWaktu){
        time=sisaWaktu;
        i++;
        letsread(time,books)
    })
}
letsread(time,books)
