//Soal 1
console.log("Soal No 1 - Objek Literal")
function arrayToObject(arr) {
  var now = new Date();
  var thisYear = now.getFullYear();

  var objTemp = {};
  var arrayOfObj = [];

  for (var x = 0; x < arr.length; x++) {
    objTemp.firstName = arr[x][0];
    objTemp.lastName = arr[x][1];
    objTemp.gender = arr[x][2];
    if (arr[x][3] == null) {
      objTemp.age = "Invalid birth year";
    } else {
      if (arr[x][3] > thisYear) {
        objTemp.age = "Invalid birth year";
      } else {
        objTemp.age = thisYear - arr[x][3];
      }
    }

    arrayOfObj.push(objTemp);
    objTemp = {}; // reset
  }
  if (arrayOfObj.length == 0) {
    console.log('""');
  } else {
    for (var x = 0; x < arrayOfObj.length; x++) {
      console.log(
        x +
          1 +
          ". " +
          arrayOfObj[x].firstName +
          " " +
          arrayOfObj[x].lastName +
          ": " +
          JSON.stringify(arrayOfObj[x])
      );
    }
  }
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);

arrayToObject([]); // ""


//Soal 2
console.log("Soal No 2 - Shopping Time")
function shoppingTime(memberId, money){
	var hasil={};
	hasil.memberId=memberId;
	hasil.money=money;
	var x=[
	[1500000,"Sepatu brand Stacattu"],
	[500000,"Baju brand Zoro"],
	[250000,"Baju brand H&N"],
	[175000,"Sweater brand Uniklooh"],
	[50000,"Casing Handphone"]];
	x.sort(function(a, b){return a - b});
	var no=0;
	var list=[];
	if(memberId==''){
		console.log("Mohon maaf, toko X hanya berlaku untuk member saja");
	}else if(money>=x[0][0]){
		money=money-x[0][0];
		list[0]=x[0][1];
		for(var a=1;a<=x.length-1;a++){
			var m_now=money-x[a][0];
			if(m_now>=0){
				list[no]=x[a][1];
				money=money-x[a][0];
			no++;}
		}
		hasil.listPurchased=list;
		hasil.changeMoney=money;
		return hasil;
	}else if(money>=x[4][0] && money<=x[0][0]){
		for(var a=1;a<=x.length-1;a++){
			var m_now=money-x[a][0];
			if(m_now>=0){
				list[no]=x[a][1];
				money=money-x[a][0];
			no++;}
		}
		hasil.listPurchased=list;
		hasil.changeMoney=money;
		return hasil;
	}else{
		console.log("Mohon maaf, uang tidak cukup");
	}
	return ""
}
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime('234JdhweRxa53', 3000000));

//Soal 3
console.log("Soal No 3 - Naik Angkot")
var pn=[['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']];
function naikAngkot(listPenumpang){
	var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	var hasil={};
	var awal=0;
	var akhir=0;
	for(var x=0;x<=listPenumpang.length-1;x++){
		for (var a=0;a<=rute.length-1;a++){
			awal=awal+1;
			if(listPenumpang[x][1]==rute[a]){
				var awal_=awal;
			}
		}
		for (var a=0;a<=rute.length-1;a++){
			akhir=akhir+1;
			if(listPenumpang[x][2]==rute[a]){
				var akhir_=akhir;
			}
		}
		hasil.penumpang=listPenumpang[x][0];
		hasil.naikDari=listPenumpang[x][1];
		hasil.tujuan=listPenumpang[x][2];
		hasil.bayar=2000*(akhir_-awal_);
		console.log(hasil);
	}
	return "";
}
console.log(naikAngkot(pn))
