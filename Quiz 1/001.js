function bandingkan(num1, num2) {
  if (num1 == null) {
    num1 = 0;
  }

  if (num2 == null) {
    num2 = 0;
  }

  var n1 = parseInt(num1);
  var n2 = parseInt(num2);

  if (n1 < 0 || n2 < 0 || (n1 < 0 && n2 < 0)) {
    return -1;
  } else {
    if (n1 == n2) {
      return -1;
    } else if (n1 > n2) {
      return n1;
    } else {
      return n2;
    }
  }
}

function balikString(str) {
  var reverseStr = "";
  for (var x = str.length - 1; x >= 0; x--) {
    reverseStr += str[x];
  }

  return reverseStr;
}

function palindrome(str) {
  var reverseStr = "";
  for (var x = str.length - 1; x >= 0; x--) {
    reverseStr += str[x];
  }

  if (str == reverseStr) {
    return true;
  } else {
    return false;
  }
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")); // 18

// TEST CASES BalikString
console.log(balikString("abcde")); // edcba
console.log(balikString("rusak")); // kasur
console.log(balikString("racecar")); // racecar
console.log(balikString("haji")); // ijah

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah")); // true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false
