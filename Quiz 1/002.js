function DescendingTen(num) {
  if (num != null) {
    var strNum = "";
    for (var x = 0; x < 10; x++) {
      strNum += String(num) + " ";
      num--;
    }
    return strNum;
  } else {
    return -1;
  }
}

function AscendingTen(num) {
  if (num != null) {
    var strNum = "";
    for (var x = 0; x < 10; x++) {
      strNum += String(num) + " ";
      num++;
    }
    return strNum;
  } else {
    return -1;
  }
}

function ConditionalAscDesc(reference, check) {
  if (reference != null && check != null) {
    if (check % 2 == 1) {
      var strNum = "";
      for (var x = 0; x < 10; x++) {
        strNum += String(reference) + " ";
        reference++;
      }
      return strNum;
    } else {
      var strNum = "";
      for (var x = 0; x < 10; x++) {
        strNum += String(reference) + " ";
        reference--;
      }
      return strNum;
    }
  } else {
    return -1;
  }
}

function ularTangga(){
	var z='';
	var x=100;
	for(var a=1;a<=10;a++){
		if( a % 2 == 0){
			z='';
			for(var b=x-9;b<=x;b++){
				z+=b+" ";
			}
			console.log(z);
			x=b-11;
		}else{
			z='';
			for(var ai=x;ai>=x-9;ai--){
				z+=ai+" ";
			}
			console.log(z);
			x=ai;
		}

	}return "";
}

// TEST CASES Descending Ten
console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)); // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()); // -1

// TEST CASES Ascending Ten
console.log(AscendingTen(11)); // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)); // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()); // -1

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)); // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)); // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)); // -1
console.log(ConditionalAscDesc()); // -1

// TEST CASE Ular Tangga
console.log(ularTangga());

