//soal no 1
var a = 2
var b = 0
var c = 22
console.log("Soal No 1")
console.log(" ")
console.log("LOOPING PERTAMA")
while(b < 20) {
  b += a;
  console.log(b + " - I love coding" )
}
console.log(" ")
console.log("LOOPING KEDUA")
while(c > 2) {
  c -= a;
  console.log(c + " - I will be a mobile developer" )
}
console.log(" ")
//soal no 2
console.log("Soal No 2")
var num = 1;

for(b = num; b <= 20; b++){
  if((b%3) === 0){
      console.log(b + ' - I love Coding')
  }else if((b%2) === 0){
    console.log(b + ' - Berkualitas');
  }else {
    console.log(b + ' - Santai')
    }
}
console.log(" ")
//soal no 3
console.log("Soal No 3")
var persegi = "####";
for(const char of persegi){
  console.log(char.repeat(8));
}
console.log(" ")
//soal no 4
console.log("Soal No 4");
var pagar = '#';
for(var alas = 1; alas <= 7; alas++){
  console.log(pagar)
  pagar = pagar + '#'
}
console.log(" ")
//soal no 5
console.log("Soal No 5");
for(i=1; i<=8; i++){
  if((i%2)===1){
    console.log(' # # # #')
  }else
  console.log('# # # #')
}